sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function(Controller, MessageBox) {
	"use strict";

	return Controller.extend("zcom.sap.crbpCreateBUPAREQ.controller.S0", {

		onInit: function() {
		
	
		
		
	
		},
		formatPriority: function(sValue){
			if(sValue=='Success')
			return "Low";
			else
			return "High";
		},
		

	
		formatErrors: function(sValue){
			
			if(sValue[0]=='S')
			{
					var data={
			status1:"APPROVED",			
			status2:"Success"
		};
		
		var sModel=new sap.ui.model.json.JSONModel();
		
		sModel.setData(data);
		this.getView().setModel(sModel,"sMOD");
			}
			else
			{
					var data={
			status1:"NOT APPROVED",			
			status2:"Error"
		};
		
		var sModel=new sap.ui.model.json.JSONModel();
		
		sModel.setData(data);
		this.getView().setModel(sModel,"sMOD");
			}
		
	
			
		var regex = new RegExp("\\$E", 'g');

			sValue = sValue.replace(regex, '</br>');
			sValue = sValue.replace(regex, '$');
			return sValue;
			
		}

	});
});